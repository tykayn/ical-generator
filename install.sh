#!/bin/bash
echo "checking requirements";
composer --version
yarn --version
acl --varsion
sudo apt install acl

echo "installing symfony project for ubuntu environnement";
php -r "copy('https://getcomposer.org/download/1.7.1/composer.phar', 'composer-setup.php');"
php -r "if (hash_file('SHA256', 'composer-setup.php') === '1c0e95dc3f33985f9eeabb6f57896c0f9d46b7c9e70ad7bf2210a5508869a8fa') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"

composer install && yarn install;

echo "fix file permissions";
HTTPDUSER=$(ps axo user,comm | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1)

sudo setfacl -dR -m u:"$HTTPDUSER":rwX -m u:$(whoami):rwX var
sudo setfacl -R -m u:"$HTTPDUSER":rwX -m u:$(whoami):rwX var

echo "update database";
php bin/console doctrine:schema:update --dump-sql
php bin/console doctrine:schema:update --force
