<?php

namespace AppBundle\Services;

use DateTime;
use Doctrine\ORM\EntityManagerInterface as EM;

class UploadService {


	private $em;

	/**
	 * UploadService constructor.
	 *
	 *  $em
	 */
	public function __construct(
		EM $em
	) {
		$this->em = $em;

	}

	public function uploadSMS( $file ) {

	}

	public function examineLineCalendar( $line ) {

		$results  = [
			'line'              => $line,
			'key'               => "",
			'value'             => "",
			'dateString'        => "",
			'dateObject'        => "",
			'isNewEvent'        => null,
			'isClosingEvent'    => null,
			'isClosingCalendar' => null,
		];
		$boom     = explode( ':', $line );
		$elements = count( $boom );
		$key      = "";
		$value    = "";
		if ( $elements > 0 ) {
			$key = $boom[ 0 ];
		}
		if ( $key == 'DTSTART;VALUE=DATE' ) {
			$key = 'DTSTART';
		}
		if ( $key == 'DTEND;VALUE=DATE' ) {
			$key = 'DTEND';
		}
		if ( isset( $boom[ 1 ] ) ) {
			$value = $boom[ 1 ];
		}
		$results[ 'key' ]   = $key;
		$results[ 'value' ] = $value;
		$isNewEvent         = '';
		$isClosingEvent     = '';
		$veventSearch       = trim( "VEVENT 
" );
		$searchEventValue   = substr( trim( $value ), 0, 6 );
		if ( $key == 'BEGIN' ) {
			if ( $searchEventValue == $veventSearch ) {
				$isNewEvent = true;
			}
		}
		if ( $key == 'END' ) {
			if ( $searchEventValue == $veventSearch ) {
				$isClosingEvent = true;
			}
		}

		$results[ 'isNewEvent' ]        = $isNewEvent;
		$results[ 'isClosingEvent' ]    = $isClosingEvent;
		$results[ 'isClosingCalendar' ] = ( $key === 'END' && $value === "CALENDAR" );

		$dateKeys = [ 'DTSTART', 'DTEND' ];
		if ( in_array( $key, $dateKeys ) ) {
			// could be DTSTART;VALUE=DATE:20120616 -> 2012/06/16
			$results[ 'dateString' ] = $value;
			$dateObjectConverted     = $this->convertDateStringToObject( $value );

			$results[ 'dateObject' ] = $dateObjectConverted;
		} elseif ( $key === 'DTSTAMP' ) {
			// could be DTSTAMP:20180813T101453Z
			$results[ 'dateString' ] = $value;
			$dateObjectConverted     = $this->convertDateStringToObject( $value );

			$results[ 'dateObject' ] = $dateObjectConverted;
		}

		return $results;
	}

	/**
	 * @param $dateString
	 *
	 * @return bool|DateTime
	 */
	public function convertDateStringToObject( $dateString ) {
		$dateString = trim( str_replace( [ 'T', 'Z' ], [ ' ', '' ], $dateString ) );

		$lengthDate = strlen( $dateString );

		$dateString = substr_replace( $dateString, '-', 4, 0 );
		$dateString = substr_replace( $dateString, '-', 7, 0 );
		if ( $lengthDate == 8 ) {
			// short version
			$formatSearch = 'Y-m-d';
		} else {
			// long version

			$dateString = substr_replace( $dateString, ' ', 10, 0 );
			$dateString = substr_replace( $dateString, ':', 14, 0 );
			$dateString = substr_replace( $dateString, ':', 17, 0 );
			$dateString = trim( $dateString );

			$formatSearch = 'Y-m-d H:i:s';
		}
//		echo "<BR/><PRE>";
//		var_dump( $lengthDate );
//		var_dump( $dateString );
//		var_dump( $formatSearch );
//
//		var_dump( $d );
//		echo "</PRE>";

		$d = DateTime::createFromFormat( $formatSearch, $dateString );

		return $d;
	}

	public function findContact( $guessElement ) {

	}
}
