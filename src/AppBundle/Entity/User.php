<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * User
 *
 * @ORM\Table(name="custom_user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 */
class User extends BaseUser {
	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;


	/**
	 * @var
	 * @ORM\OneToMany(targetEntity="Contact", mappedBy="owner")
	 */
	protected $contacts;
	/**
	 * @var
	 * @ORM\OneToMany(targetEntity="Evenement", mappedBy="owner")
	 */
	protected $events;
	/**
	 * @var
	 * @ORM\OneToMany(targetEntity="Sms", mappedBy="owner")
	 */
	protected $sms;
	/**
	 * @var
	 * @ORM\OneToMany(targetEntity="Conversation", mappedBy="owner")
	 */
	protected $conversations;
	/**
	 * @var
	 * @ORM\OneToMany(targetEntity="Research", mappedBy="owner")
	 */
	protected $researchs;

	/**
	 * Get id
	 *
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Add contact
	 *
	 * @param \AppBundle\Entity\Contact $contact
	 *
	 * @return User
	 */
	public function addContact( \AppBundle\Entity\Contact $contact ) {
		$this->contacts[] = $contact;

		return $this;
	}

	/**
	 * Remove contact
	 *
	 * @param \AppBundle\Entity\Contact $contact
	 */
	public function removeContact( \AppBundle\Entity\Contact $contact ) {
		$this->contacts->removeElement( $contact );
	}

	/**
	 * Get contacts
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getContacts() {
		return $this->contacts;
	}

	/**
	 * Add event
	 *
	 * @param \AppBundle\Entity\Evenement $event
	 *
	 * @return User
	 */
	public function addEvent( \AppBundle\Entity\Evenement $event ) {
		$this->events[] = $event;

		return $this;
	}

	/**
	 * Remove event
	 *
	 * @param \AppBundle\Entity\Evenement $event
	 */
	public function removeEvent( \AppBundle\Entity\Evenement $event ) {
		$this->events->removeElement( $event );
	}

	/**
	 * Get events
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getEvents() {
		return $this->events;
	}

	/**
	 * Add sm
	 *
	 * @param \AppBundle\Entity\Sms $sm
	 *
	 * @return User
	 */
	public function addSms( \AppBundle\Entity\Sms $sm ) {
		$this->sms[] = $sm;

		return $this;
	}

	/**
	 * Remove sm
	 *
	 * @param \AppBundle\Entity\Sms $sm
	 */
	public function removeSms( \AppBundle\Entity\Sms $sm ) {
		$this->sms->removeElement( $sm );
	}

	/**
	 * Get sms
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getSms() {
		return $this->sms;
	}

	/**
	 * Add conversation
	 *
	 * @param \AppBundle\Entity\Conversation $conversation
	 *
	 * @return User
	 */
	public function addConversation( \AppBundle\Entity\Conversation $conversation ) {
		$this->conversations[] = $conversation;

		return $this;
	}

	/**
	 * Remove conversation
	 *
	 * @param \AppBundle\Entity\Conversation $conversation
	 */
	public function removeConversation( \AppBundle\Entity\Conversation $conversation ) {
		$this->conversations->removeElement( $conversation );
	}

	/**
	 * Get conversations
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getConversations() {
		return $this->conversations;
	}

	/**
	 * Add research
	 *
	 * @param \AppBundle\Entity\Research $research
	 *
	 * @return User
	 */
	public function addResearch( \AppBundle\Entity\Research $research ) {
		$this->researchs[] = $research;

		return $this;
	}

	/**
	 * Remove research
	 *
	 * @param \AppBundle\Entity\Research $research
	 */
	public function removeResearch( \AppBundle\Entity\Research $research ) {
		$this->researchs->removeElement( $research );
	}

	/**
	 * Get researchs
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getResearchs() {
		return $this->researchs;
	}

	/**
	 * Add sm.
	 *
	 * @param \AppBundle\Entity\Sms $sm
	 *
	 * @return User
	 */
	public function addSm( \AppBundle\Entity\Sms $sm ) {
		$this->sms[] = $sm;

		return $this;
	}

	/**
	 * Remove sm.
	 *
	 * @param \AppBundle\Entity\Sms $sm
	 *
	 * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
	 */
	public function removeSm( \AppBundle\Entity\Sms $sm ) {
		return $this->sms->removeElement( $sm );
	}
}
