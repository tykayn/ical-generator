<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Conversation
 *
 * @ORM\Table(name="conversation")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ConversationRepository")
 */
class Conversation {
	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="uid", type="string", length=400, nullable=true, unique=true)
	 */
	private $uid;
	/**
	 * @var string
	 * place where the conversation happens, irc, msn, aol...
	 * @ORM\Column(name="place", type="string", length=400, nullable=true)
	 */
	private $place;
	/**
	 * @var string
	 * place where the conversation happens, irc, msn, aol...
	 * @ORM\Column(name="body", type="text", nullable=false)
	 */
	private $body;
	/**
	 * @var string
	 * is the conversation started by the other contact or me?
	 * @ORM\Column(name="started_by_other", type="boolean", nullable=true)
	 */
	private $startedByOther;

	/**
	 * @var string
	 * original email inputed with the import
	 * @ORM\Column(name="pseudo", type="string", length=400, nullable=true, unique=true)
	 */
	private $pseudo;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="start", type="datetime")
	 */
	private $start;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="end", type="datetime", nullable=true)
	 */
	private $end;

	/**
	 * @var
	 * @ORM\ManyToOne(targetEntity="User", inversedBy="conversations")
	 */
	protected $owner;
	/**
	 * @var
	 * @ORM\ManyToOne(targetEntity="Contact", inversedBy="conversations")
	 */
	protected $contact;
	/**
	 * @var
	 * @ORM\OneToOne(targetEntity="Evenement")
	 */
	protected $evenement;

	/**
	 * Get id
	 *
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set uid
	 *
	 * @param string $uid
	 *
	 * @return Conversation
	 */
	public function setUid( $uid ) {
		$this->uid = $uid;

		return $this;
	}

	/**
	 * Get uid
	 *
	 * @return string
	 */
	public function getUid() {
		return $this->uid;
	}

	/**
	 * Set start
	 *
	 * @param \DateTime $start
	 *
	 * @return Conversation
	 */
	public function setStart( $start ) {
		$this->start = $start;

		return $this;
	}

	/**
	 * Get start
	 *
	 * @return \DateTime
	 */
	public function getStart() {
		return $this->start;
	}

	/**
	 * Set end
	 *
	 * @param \DateTime $end
	 *
	 * @return Conversation
	 */
	public function setEnd( $end ) {
		$this->end = $end;

		return $this;
	}

	/**
	 * Get end
	 *
	 * @return \DateTime
	 */
	public function getEnd() {
		return $this->end;
	}

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->owner = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * Add owner
	 *
	 * @param \AppBundle\Entity\User $owner
	 *
	 * @return Conversation
	 */
	public function addOwner( \AppBundle\Entity\User $owner ) {
		$this->owner[] = $owner;

		return $this;
	}

	/**
	 * Remove owner
	 *
	 * @param \AppBundle\Entity\User $owner
	 */
	public function removeOwner( \AppBundle\Entity\User $owner ) {
		$this->owner->removeElement( $owner );
	}

	/**
	 * Get owner
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getOwner() {
		return $this->owner;
	}

	/**
	 * Set owner
	 *
	 * @param \AppBundle\Entity\User $owner
	 *
	 * @return Conversation
	 */
	public function setOwner( \AppBundle\Entity\User $owner = null ) {
		$this->owner = $owner;

		return $this;
	}

	/**
	 * Set evenement
	 *
	 * @param \AppBundle\Entity\Evenement $evenement
	 *
	 * @return Conversation
	 */
	public function setEvenement( \AppBundle\Entity\Evenement $evenement = null ) {
		$this->evenement = $evenement;

		return $this;
	}

	/**
	 * Get evenement
	 *
	 * @return \AppBundle\Entity\Evenement
	 */
	public function getEvenement() {
		return $this->evenement;
	}

	/**
	 * Set email.
	 *
	 * @param string|null $pseudo
	 *
	 * @return Conversation
	 */
	public function setPseudo( $pseudo = null ) {
		$this->pseudo = $pseudo;

		return $this;
	}

	/**
	 * Get email.
	 *
	 * @return string|null
	 */
	public function getPseudo() {
		return $this->pseudo;
	}

	/**
	 * Set contact.
	 *
	 * @param \AppBundle\Entity\Contact|null $contact
	 *
	 * @return Conversation
	 */
	public function setContact( \AppBundle\Entity\Contact $contact = null ) {
		$this->contact = $contact;

		return $this;
	}

	/**
	 * Get contact.
	 *
	 * @return \AppBundle\Entity\Contact|null
	 */
	public function getContact() {
		return $this->contact;
	}

	/**
	 * Set place.
	 *
	 * @param string|null $place
	 *
	 * @return Conversation
	 */
	public function setPlace( $place = null ) {
		$this->place = $place;

		return $this;
	}

	/**
	 * Get place.
	 *
	 * @return string|null
	 */
	public function getPlace() {
		return $this->place;
	}

	/**
	 * Set body.
	 *
	 * @param string $body
	 *
	 * @return Conversation
	 */
	public function setBody( $body ) {
		$this->body = $body;

		return $this;
	}

	/**
	 * Get body.
	 *
	 * @return string
	 */
	public function getBody() {
		return $this->body;
	}

	/**
	 * Set startedByOther.
	 *
	 * @param bool|null $startedByOther
	 *
	 * @return Conversation
	 */
	public function setStartedByOther( $startedByOther = null ) {
		$this->startedByOther = $startedByOther;

		return $this;
	}

	/**
	 * Get startedByOther.
	 *
	 * @return bool|null
	 */
	public function getStartedByOther() {
		return $this->startedByOther;
	}
}
