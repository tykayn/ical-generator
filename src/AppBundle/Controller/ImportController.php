<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Contact;
use AppBundle\Entity\Conversation;
use AppBundle\Entity\Evenement;
use AppBundle\Entity\Research;
use AppBundle\Entity\Sms;
use AppBundle\Services\UploadService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ImportController extends Controller {
	public $uploadService;

	/**
	 * ImportController constructor.
	 *
	 * @param UploadService $uploadService
	 */
	public function __construct( UploadService $uploadService ) {
		$this->uploadService = $uploadService;
	}

	/**
	 * @Route("/import-research", name="i_research")
	 */
	public function importResearchsAction( Request $request ) {
		$m           = $this->getDoctrine()->getManager();
		$userRepo    = $m->getRepository( 'AppBundle:User' );
		$currentUser = $this->getUser();

		$path = $_FILES[ "csvfile" ][ 'tmp_name' ];
		// read json
		$json    = file_get_contents( $path, true );
		$json    = json_decode( $json, true );
		$counter = 0;
		foreach (
			$json[ 'event' ]
			as $event
		) {
			$research = new Research();
//			if ( count( $event[ 'query' ][ 'id' ][ 0 ][ 'timestamp_usec' ] ) > 1 ) {
//				$timeStamp = $event[ 'query' ][ 'id' ][ 0 ][ 'timestamp_usec' ][ 0 ];
//				$research
//					->setMultipleDate( join( ';', $event[ 'query' ][ 'id' ][ 0 ][ 'timestamp_usec' ] ) );
//			} else {
			$timeStamp = $event[ 'query' ][ 'id' ][ 0 ][ 'timestamp_usec' ];
//			}
			$timeStamp = strval( ceil( intval( $timeStamp ) / 1000000 ) );

			$date = new \DateTime();
			$date = $date->setTimestamp( $timeStamp );
			$research
				->setDate( $date )
				->setQueryText( $event[ 'query' ][ 'query_text' ] )
				->setOwner( $currentUser );
			$currentUser->addResearch( $research );
			$m->persist( $research );
			$counter ++;
		}

		$m->flush();

		return $this->render( 'logged/dashboard.html.twig',
			[ 'counter_importations_done' => $counter ] );
	}

	/**
	 * @Route("/import-ical", name="i_ical")
	 */
	public function importIcalAction( Request $request ) {
		$m               = $this->getDoctrine()->getManager();
		$userRepo        = $m->getRepository( 'AppBundle:User' );
		$currentUser     = $this->getUser();
		$uploadService   = $this->uploadService;
		$path            = $_FILES[ "icalfile" ][ 'tmp_name' ];
		$counter         = 0;
		$counterEvents   = 0;
		$dumpContentIcal = false; // false to enable saving of events
		$icalContent     = [];
		$handle          = fopen( $path, "r" );

		$calendarDefinition = [
			'name'   => '',
			'prodID' => '',
			'scale'  => '',
		];

		$non_listed_keys = [];
		if ( $handle ) {
			// start of a calendar doc is this:
			//BEGIN:VCALENDAR
			//PRODID:-//Google Inc//Google Calendar 70.9054//EN
			//VERSION:2.0
			//CALSCALE:GREGORIAN
			//METHOD:PUBLISH
			//X-WR-CALNAME:!Q zine actuel
			//X-WR-TIMEZONE:Europe/Paris
			//BEGIN:VEVENT
			// then start the events list
			// END:VEVENT  ends event
			// END:VCALENDAR end the calendar
			$eventsStarted = false;
			$currentEvent  = new Evenement();
			while ( ( $line = fgets( $handle ) ) !== false ) {
				$lineResult = $uploadService->examineLineCalendar( $line );

				if ( $lineResult[ 'isNewEvent' ] ) {
					$eventsStarted = true;
				} else {
					// lines defining the calendar
					if ( $lineResult[ 'key' ] === 'X-WR-CALNAME' ) {
						$calendarDefinition[ 'name' ] = $lineResult[ 'value' ];
					} elseif ( $lineResult[ 'key' ] === 'PRODID' ) {
						$calendarDefinition[ 'prodID' ] = $lineResult[ 'value' ];
					} elseif ( $lineResult[ 'key' ] === 'CALSCALE' ) {
						$calendarDefinition[ 'scale' ] = $lineResult[ 'value' ];
					}
				}


				if ( ! $eventsStarted ) {

				} else {

					// populate content of the event
					switch ( $lineResult[ 'key' ] ) {
						case 'VISIBILITY':
							$currentEvent->setVisibility( $lineResult[ 'value' ] );
							break;
						case 'UID':
							$currentEvent->setUid( $lineResult[ 'value' ] );
							break;
						case 'SUMMARY':
							$currentEvent->setSummary( $lineResult[ 'value' ] );
							break;
						case 'DESCRIPTION':
//							echo "<br/> " . $lineResult[ 'value' ];
							$currentEvent->setDescription( $lineResult[ 'value' ] );
							break;

						case 'LOCATION':
							$currentEvent->setLocation( $lineResult[ 'value' ] );
							break;
						case 'RRULE':
							$currentEvent->setRepeatRules( $lineResult[ 'value' ] );
							break;
						// DATES objects
						case 'DTSTART':
							$currentEvent->setStart( $lineResult[ 'dateObject' ] );
							break;
						case 'DTEND':
							$currentEvent->setEnd( $lineResult[ 'dateObject' ] );
							break;

						// DATES strings
						case 'LAST-MODIFIED':
							$currentEvent->setDateModified( $lineResult[ 'dateString' ] );
							break;
						case 'DTSTAMP':
							$currentEvent->setDate( $lineResult[ 'dateObject' ] );
							$currentEvent->setDateOriginale( $lineResult[ 'dateString' ] );
							break;
						default:
//							echo "<br/>     xxxxxx not found " . $lineResult[ 'key' ];
							// add to non listed properties
							if ( ! isset( $non_listed_keys[ $lineResult[ 'key' ] ] ) ) {
								$non_listed_keys[ $lineResult[ 'key' ] ] = 0;
							}
							$non_listed_keys[ $lineResult[ 'key' ] ] ++;
							break;
					}


					if ( $lineResult[ 'isNewEvent' ] ) {

						// sets the calendar propertie from first lines

						$currentEvent->setCalendarName( $calendarDefinition[ 'name' ] );
						$currentEvent->setCalendarScale( $calendarDefinition[ 'scale' ] );
						$currentEvent->setProdID( $calendarDefinition[ 'prodID' ] );
					}
					if ( $lineResult[ 'isClosingEvent' ] ) {

						if ( $dumpContentIcal ) {
							// just view content
							echo "<br> $line";
						} else {
							$currentEvent->setOwner( $currentUser );
							$currentUser->addEvent( $currentEvent );
							// or save
							$m->persist( $currentEvent );
							$m->persist( $currentUser );

						}
						// pass to next event

//						echo "<br/> CREATE NEXT EVENT";
						$newEvent     = new Evenement();
						$currentEvent = $newEvent;
						$counterEvents ++;
					} elseif ( $lineResult[ 'isClosingCalendar' ] ) {

//						echo "CLOSE CALENDAR ";

					}


				}


				$counter ++;

			}

		} else {
			// error opening the file.
			die( "ERROR OPENING FILE calendar : " . $path );
		}


		fclose( $handle );
		if ( $dumpContentIcal ) {

			echo "<br> dump content: $dumpContentIcal";
		} else {
			$m->flush();
		}
//		echo "<br> NON LISTED KEYS FOR EVENTS: " . count( $non_listed_keys );
//		var_dump( $non_listed_keys );

		echo "<br> counter events : $counterEvents";
//		echo "<br> counter lines : $counter";

//		die();

		return $this->render( 'logged/dashboard.html.twig',
			[ 'counter_importations_done' => $counterEvents ] );
	}

	/**
	 * @Route("/import-fbcomments", name="i_fbcomments")
	 */
	public
	function importFBcommentsAction(
		Request $request
	) {
		$m           = $this->getDoctrine()->getManager();
		$currentUser = $this->getUser();
		$counter     = 0;

// TODO
		return $this->render( 'logged/dashboard.html.twig',
			[ 'counter_importations_done' => $counter ] );
	}

	/**
	 * @Route("/import-trillian-msn", name="i_t_msn")
	 */
	public
	function importTrillianMSNAction(
		Request $request
	) {

		$enableSAVE = false;
		$enableMOCK = true;

		$m            = $this->getDoctrine()->getManager();
		$repoContacts = $m->getRepository( 'AppBundle:Contact' );

		$counter = 0;

		$currentUser = $this->getUser();


		if ( $enableMOCK ) {
			/**
			 * mock for dev
			 */
			$nameSearch = "hikasauwr";
			$lines      = "﻿Session Start (suisse.Fantasya.org:hika): Fri Jun 17 16:29:31 2005
<hika> bonjour o°___°o
*** You are now known as Tk|SOS
Session Close (hika): Fri Jun 17 21:26:25 2005


Session Start (Osmove.Fantasya.org:hika): Tue Jun 28 00:23:47 2005
<hika> lut *çç*
<hika> *^^*
[00:36] *** You are now known as Tykay
[00:36] <Tykay> huhu 
*** hika: No such nick/channel
Session Close (hika): Tue Jun 28 00:36:26 2005";

		} else {

			$nameSearch = $request->request->get( 'destinataire' );
			$path       = $_FILES[ "file" ][ 'tmp_name' ];
			// read json
			$lines = file_get_contents( $path, true );

		}

		$linesSeparated = explode( "\n", $lines );
		echo '$linesSeparated ' . count( $linesSeparated ) . ' <br/>';
		// split the file in lines
		$counter      = 0;
		$conversation = new conversation();

		$searchPseudoInChevrons     = '/.*<(.*)>.*/';
		$searchPseudoInSessionStart = '/:(.*)\)/';
		$searchPlaceInSessionStart  = '/(\(.*:)/';
		foreach (
			$linesSeparated
			as $line
		) {
			echo '<br>ligne ______' . $line . '<br>';
			//Session Start (Osmove.Fantasya.org:hika): Tue Jun 28 00:23:47 2005
			if ( strpos( $line, 'Session Start' ) !== false ) {
				echo 'nouvelle conversation  <br/> ______';
				// find the start of a conversation
				$conversation = new Conversation();
				$conversation->setOwner( $currentUser );
				$currentUser->addConversation( $conversation );
				$boom          = explode( ':', $line, 2 );
				$dateConverted = date_create_from_format( 'D M d H:i:s Y', trim( $boom[ 1 ] ) );
				// ﻿Session Start (osmoze.fantasya.org:Jiyone): Wed Mar 10 20:26:12 2004
				// find place osmoze.fantasya.org TODO
				// find pseudo Jiyone TODO
				preg_match( $searchPlaceInSessionStart, $line, $place );
//				echo '<pre>';
//				var_dump( $place );
//				echo '</pre>';
//				$boom  = explode( ':', $place );
//				if ( isset( $boom[ 0 ] ) ) {
//					$place = $boom[ 0 ];
//				}
				$pseudo = preg_match( $searchPseudoInSessionStart, $line );
				if ( isset( $pseudo[ 1 ] ) ) {
					$pseudo = $pseudo[ 1 ];
				} else {
					$pseudo = $nameSearch;
				}

//				echo '<br/> $place ' . $place;
				echo '<br/> $pseudo ' . $pseudo;

				$conversation
					->setStart( $dateConverted )
					->setPseudo( $pseudo )//					->setPlace( $place )
				;


// find corresponding contact by email
				// find corresponding contact by pseudo
				if ( strpos( $nameSearch, '@' ) ) {
					$foundContact = $repoContacts->findByEmail( $nameSearch );
				} else {
					$foundContact = $repoContacts->findByFirstName( $nameSearch );
				}
				// if contact not found create it
				if ( ! $foundContact ) {

					echo '<br/> nouveau contact  ';
					$contact = new Contact();
					if ( strpos( $nameSearch, '@' ) ) {

						$contact->setEmail( $nameSearch );
					} else {
						$contact->setFirstName( $nameSearch );
					}
					$foundContact = $contact;
				} else {
					echo 'contact existant ' . $foundContact->getFirstName . '<br/>';
				}
				if ( $enableSAVE ) {

					$foundContact->addConversation( $conversation );
					$conversation->setContact( $foundContact );
					$m->persist( $foundContact );
				}

			} /** text sent during offline time
			 * <hika> bonjour o°___°o
			 */
//			elseif () {
//
//			} /** status change
//			 * [00:36] *** You are now known as Tykay
//			 */
//			elseif () {
//
//			} /** info
//			 * *** hika: No such nick/channel
//			 */
//			elseif () {
//
//			} /** info
//			 * [00:36] *** You are now known as Tykay
//			 */
//			elseif () {
//
//			}
			// Session Close (hika): Tue Jun 28 00:36:26 2005
			// Session Close: Fri Nov 23 01:27:28 2007
			elseif ( strpos( $line, 'Session Close' ) !== false ) {
				echo 'fin de conversation  <br/>' . $line . '<br/> ______';
				$boom          = explode( ':', $line, 2 );
				$dateConverted = date_create_from_format( 'D M d H:i:s Y', trim( $boom[ 1 ] ) );
				$conversation->setEnd( $dateConverted );
				// convert date

				if ( $enableSAVE ) {
					$m->persist( $conversation );
				}
				// find the end of a conversation
				$counter ++;
			} else {

				$oldBody = $conversation->getBody();
				if ( ! $oldBody ) {
					// first content,
					// find hika in "<hika> bonjour o°___°o"
					// is it started by the other people?
				}
				// add content to body
				$conversation->setBody( $oldBody . '
				' . $line );
			}


		}
		if ( $enableSAVE ) {
			$m->persist( $currentUser );
			$m->flush();
		}

		die( $counter . ' importations' );

		return $this->render( 'logged/dashboard.html.twig',
			[ 'counter_importations_done' => $counter ] );
	}

	/**
	 * import event from csv
	 * @Route("/i_event_generic", name="i_event_generic")
	 */
	public
	function importEventsGeneralAction(
		Request $request
	) {
		$m           = $this->getDoctrine()->getManager();
		$currentUser = $this->getUser();
		$counter     = 0;

// TODO
		return $this->render( 'logged/dashboard.html.twig',
			[ 'counter_importations_done' => $counter ] );
	}

	/**
	 * @Route("/i_msn-xml", name="i_msn_xml")
	 */
	public
	function importMsnXmlAction(
		Request $request
	) {
		$m           = $this->getDoctrine()->getManager();
		$currentUser = $this->getUser();
		$counter     = 0;

// TODO
		return $this->render( 'logged/dashboard.html.twig',
			[ 'counter_importations_done' => $counter ] );
	}

	/**
	 * @Route("/import-fbcomments", name="i_fbcomments")
	 * created with IFTTT with google drive
	 */
	public
	function importPhoneCallsAction(
		Request $request
	) {
		$counter = 0;

		return $this->render( 'logged/dashboard.html.twig',
			[ 'counter_importations_done' => $counter ] );
	}

	/**
	 * @Route("/import-google-contacts", name="i_gcontacts")
	 */
	public
	function importGoogleContactsAction(
		Request $request
	) {
		$m           = $this->getDoctrine()->getManager();
		$userRepo    = $m->getRepository( 'AppBundle:User' );
		$currentUser = $this->getUser();
		// read csv

		$path = $_FILES[ "csvfile" ][ 'tmp_name' ];
//		var_dump( $path );
		$handle  = fopen( $path, "r" );
		$counter = 0;
		while ( ( $row = fgetcsv( $handle, 0, "," ) ) !== false ) {
			//Dump out the row for the sake of clarity.
			if ( $counter == 0 ) {
				$counter ++;
				continue;
			}
//			echo "<br/>import du contact  " . $row[ 0 ] . $row[ 1 ] . $row[ 2 ] . $row[ 75 ];
			$contact = new Contact();
			$contact
				->setFirstName( $row[ 0 ] )
				->setMiddleName( $row[ 1 ] )
				->setLastName( $row[ 2 ] )
				->setBirthDay( $row[ 8 ] )
				->setNotes( $row[ 13 ] )
				->setEmail( $row[ 14 ] )
				->setEmail2( $row[ 15 ] )
				->setEmail3( $row[ 16 ] )
				->setTel( $this->simplifyTelNumbers( $row[ 17 ] ) )
				->setHomePhone( $this->simplifyTelNumbers( $row[ 18 ] ) )
				->setTelMobile( $this->simplifyTelNumbers( $row[ 20 ] ) )
				->setHomeAddress( $row[ 23 ] )
				->setHomeCity( $row[ 28 ] )
				->setHomePostalCode( $row[ 30 ] )
				->setHomeCountry( $row[ 31 ] )
				->setCompany( $row[ 42 ] )
				->setUser1( $row[ 73 ] )
//				->setUser2( $row[ 76 ] )
//				->setUser3( $row[ 73 ] )
				->setCategories( $row[ 87 ] )
				->setOwner( $currentUser );
			$currentUser->addContact( $contact );
			$m->persist( $contact );
			$counter ++;
		}
		$m->persist( $currentUser );
		$m->flush();

		// notification info
		return $this->render( 'logged/dashboard.html.twig',
			[ 'counter_importations_done' => $counter ] );
	}

	/**
	 * @Route("/import-sms", name="i_sms")
	 */
	public
	function importSMSAction(
		Request $request
	) {
		$m           = $this->getDoctrine()->getManager();
		$userRepo    = $m->getRepository( 'AppBundle:User' );
		$currentUser = $this->getUser();
		// read csv

		$path = $_FILES[ "csvfile" ][ 'tmp_name' ];
//		var_dump( $path );
		$handle  = fopen( $path, "r" );
		$counter = 0;
		while ( ( $row = fgetcsv( $handle, 0, "," ) ) !== false ) {
			//Dump out the row for the sake of clarity.
			if ( $counter == 0 ) {
				$counter ++;
				continue;
			}
			$sms = new Sms();
//			var_dump( $row );
			$sms
				->setSenderTel( $row[ 0 ] )
				->setRecieverTel( 'me' )
				->setContent( $row[ 1 ] )
				->setDateRaw( $row[ 2 ] )
				->setOwner( $currentUser );
			$currentUser->addSms( $sms );
			$m->persist( $sms );
			$m->persist( $currentUser );
			$counter ++;
		}
		$m->flush();
//		$content = file_get_contents( $data );
//		$csvAsArray = array_map( 'str_getcsv', file( $file_tmp ) );
//		var_dump( $content );
//		var_dump( count( $csvAsArray ) );
		// for each query
		// convert google timestamp by dividing it by 1000
		// make a new research
		// make a new event
		// link research with owner and event
		// save all
		// flush
		// notification info
		return $this->render( 'logged/dashboard.html.twig',
			[ 'counter_importations_done' => $counter ] );
	}


	/**
	 * @param String $tel
	 *
	 * @return mixed
	 */
	public
	function simplifyTelNumbers(
		$tel
	) {
		return str_replace( ' ', '', trim( $tel ) );
	}
}
